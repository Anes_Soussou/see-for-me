import React from "react";
import Svg, { Path } from "react-native-svg";

function RappelIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width ? props.width :"16.194"}
      height={props.height ? props.height :"16.889"}
      viewBox="0 0 16.194 16.889"
    >
      <Path
       fill={props.fill? props.fill : "none"}
       
        d="M19.379 13.144L17.7 11.463V8.6a6.271 6.271 0 00-5.6-6.228 2.626 2.626 0 00-1.246 0A6.321 6.321 0 005.242 8.6v2.865l-1.681 1.679a.567.567 0 00-.187.436v1.868a.585.585 0 00.626.623h4.356a3.114 3.114 0 106.228 0h4.359a.585.585 0 00.623-.623V13.58a.567.567 0 00-.187-.436zm-7.909 4.795a1.868 1.868 0 01-1.87-1.868h3.737a1.868 1.868 0 01-1.868 1.868zm6.85-3.114H4.619v-1L6.3 12.148a.567.567 0 00.187-.436V8.6a4.982 4.982 0 019.964 0v3.114a.567.567 0 00.187.436l1.682 1.682z"
        transform="translate(-3.373 -2.296)"
      ></Path>
    </Svg>
  );
}

export default RappelIcon;
