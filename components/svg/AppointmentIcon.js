import React from "react";
import Svg, { G ,Rect ,Path } from "react-native-svg";

function AppointmentIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width ? props.width : "31"}
      height={props.height ? props.height : "33"}
      viewBox="0 0 31 33"
      style={props.style ? props.style : {}}
    >
      <G transform="translate(-63.947 -20.83)">
        <G
          fill={props.fill ? props.fill : "none"}
          stroke={props.stroke ? props.stroke : "#fff"}
          strokeLinecap="round"
          strokeWidth="1"
          data-name="Rectangle 22558"
          transform="translate(67.947 33.83)"
        >
          <Rect width="7" height="7" stroke="none" rx="1"></Rect>
          <Rect width="6" height="6" x="0.5" y="0.5" rx="0.5"></Rect>
        </G>
        <G
          fill={props.fill ? props.fill : "none"}
          stroke={props.stroke ? props.stroke : "#fff"}
          strokeWidth="1.5"
          data-name="Rectangle 22559"
          transform="translate(63.947 23.83)"
        >
          <Rect width="31" height="30" stroke="none" rx="3"></Rect>
          <Rect width="29.5" height="28.5" x="0.75" y="0.75" rx="2.25"></Rect>
        </G>
        <Path
          fill={props.fill ? props.fill : "none"}
          stroke={props.stroke ? props.stroke : "#fff"}
          strokeWidth="1.5"
          d="M30.012 0L0 0"
          data-name="Ligne 33"
          transform="translate(64.441 30.268)"
        ></Path>
        <Rect
          width="2"
          height="4"
          fill="#fff"
          data-name="Rectangle 22560"
          rx="1"
          transform="translate(69.947 20.83)"
        ></Rect>
        <Rect
          width="2"
          height="4"
          fill="#fff"
          data-name="Rectangle 22561"
          rx="1"
          transform="translate(86.947 20.83)"
        ></Rect>
      </G>
    </Svg>
  );
}

export default AppointmentIcon;
