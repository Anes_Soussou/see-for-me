import React from "react";
import Svg, {G,Path } from "react-native-svg";


function AddIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width? props.width : "20.333"}
      height={props.height? props.height : "20.333"}
      viewBox="0 0 20.333 20.333"
      style={props.style? props.style : {}}
    >
      <G
        fill={props.fill? props.fill : "none"}
        stroke={props.stroke? props.stroke : "#fff"}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="3"
        data-name="Icon feather-plus"
        transform="translate(-6 -6)"
      >
        <Path d="M16.167 7.5v17.333" data-name="Tracé 2322"></Path>
        <Path d="M7.5 16.167h17.333" data-name="Tracé 2323"></Path>
      </G>
    </Svg>
  );
}

export default AddIcon;
