import React from "react";
import Svg, { Path } from "react-native-svg";

function DescriptionIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width ? props.width : "19.726"}
      height={props.height ? props.height : "17.26"}
      viewBox="0 0 19.726 17.26"
      style={props.style ? props.style : {}}
    >
      <Path
        fill={props.fill ? props.fill : "#28367a"}
        d="M0 0v2.466h19.726V0zm0 4.931V7.4h14.794V4.931zm0 4.932v2.466h19.726V9.863zm0 4.931v2.466h14.794v-2.466z"
        data-name="Icon open-align-left"
      ></Path>
    </Svg>
  );
}

export default DescriptionIcon;
