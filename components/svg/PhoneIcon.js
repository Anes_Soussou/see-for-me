import React from "react";
import Svg, {Path } from "react-native-svg";

function PhoneIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width ? props.width : "21.413"}
      height={props.height ? props.height : "21.411"}
      viewBox="0 0 21.413 21.411"
      style={props.style ? props.style : {}}
    >
      <Path
        fill={props.fill? props.fill : "none"}
        stroke={props.stroke? props.stroke : "#28367a"}
        strokeWidth="1.5"
        d="M20.358 16.497a17.217 17.217 0 00-3.593-2.4c-1.077-.517-1.472-.506-2.234.042-.635.459-1.045.885-1.775.725a10.591 10.591 0 01-3.567-2.638 10.5 10.5 0 01-2.638-3.567c-.155-.735.272-1.141.725-1.775.549-.762.565-1.157.042-2.234a16.867 16.867 0 00-2.4-3.594C4.134.272 3.958.443 3.527.597a7.861 7.861 0 00-1.274.677 3.845 3.845 0 00-1.53 1.615c-.3.656-.656 1.877 1.136 5.065a28.253 28.253 0 004.968 6.627l.005.005.005.005a28.363 28.363 0 006.623 4.97c3.188 1.792 4.409 1.439 5.065 1.136a3.776 3.776 0 001.615-1.53 7.968 7.968 0 00.677-1.274c.155-.432.331-.608-.459-1.391z"
      ></Path>
    </Svg>
  );
}

export default PhoneIcon;
