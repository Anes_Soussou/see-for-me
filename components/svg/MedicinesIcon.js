import React from "react";
import Svg, { Path } from "react-native-svg"
import PropTypes from 'prop-types';


function MedicinesIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={ props.width ? props.width : "23.305"}
      height={ props.height ? props.height : "20.798"}
      viewBox="0 0 23.305 20.798"
      style={props.style ?props.style : {}}
    >
      <Path
        fill={props.fill ?props.fill : "none"}
        stroke={props.stroke ? props.stroke :"#000"}
        strokeWidth="1"
        d="M22.511 6.922a1.126 1.126 0 00-.832-.368H1.626a1.126 1.126 0 00-1.125 1.23l1.07 11.492a1.126 1.126 0 001.13 1.022h17.908a1.126 1.126 0 001.121-1.021l1.071-11.493a1.125 1.125 0 00-.29-.862zm-7.1 11.019h-7.51a1.126 1.126 0 01-1.126-1.126 2.775 2.775 0 012.772-2.772h.808a2.693 2.693 0 112.6 0h.808a2.776 2.776 0 012.771 2.679v.093a1.126 1.126 0 01-1.127 1.125zm5.232-13.482a.844.844 0 01-.844.844H3.511a.844.844 0 110-1.688h16.284a.844.844 0 01.844.843zm-2.229-3.114a.844.844 0 01-.844.844H5.739a.844.844 0 010-1.688h11.827a.844.844 0 01.844.843z"
      ></Path>
    </Svg>
  );
}
MedicinesIcon.prototype={
    height: PropTypes.number,
    width: PropTypes.number,
    fill: PropTypes.string,
    stroke: PropTypes.string,
}
export default MedicinesIcon;
