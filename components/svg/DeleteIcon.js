import React from "react";
import Svg, {Path } from "react-native-svg";


function DeleteIcon(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width ? props.width : "9.999"}
      height={props.height ? props.height : "12.856"}
      viewBox="0 0 9.999 12.856"
    >
      <Path
        fill={ props.fill ? props.fill :"#fff"}
        d="M8.214 15.927a1.433 1.433 0 001.428 1.428h5.714a1.433 1.433 0 001.428-1.428v-8.57h-8.57zM17.5 5.214H15l-.715-.714h-3.571L10 5.214H7.5v1.429h10z"
        data-name="Icon material-delete"
        transform="translate(-7.5 -4.5)"
      ></Path>
    </Svg>
  );
}

export default DeleteIcon;
