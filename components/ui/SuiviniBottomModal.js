import React from 'react';
import {
  Modal,
  Pressable,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../constants/Colors';

const SuiviniBottomModal = function (props) {
  return (
    <Modal animationType="slide" transparent={true} {...props}>
      <Pressable style={styles.overlay} onPress={props.onClickOutside}>
        <TouchableWithoutFeedback style={styles.touchableWithoutFeedback}>
          <SafeAreaView style={styles.safeAreaView}>
            <Pressable
              style={({ pressed }) => [
                styles.closeButton,
                {
                  backgroundColor: pressed ? Colors.background : '#fff',
                },
              ]}
              onPress={props.onClose}
            >
              <Icon name="window-close" size={18} color={Colors.primary} />
            </Pressable>
            {props.children}
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </Pressable>
    </Modal>
  );
};

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: Colors.overlay,
  },
  safeAreaView: {
    backgroundColor: 'white',
    width: '100%',
    padding: 15,
    paddingTop: 30,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
    minHeight: 200,
  },
  closeButton: {
    position: 'absolute',
    top: 10,
    right: 10,
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
});

export default SuiviniBottomModal;
