export default Colors = {
  primary: "#1F82A9",
  secondary: "red",
  success: "#52A500",
  azure: "#E7E7E7",
  border: "#CCCCCC",
  text: "#515151",
  grey: "#8E8E93",
  greyB5B5B5: "#B5B5B5",
  lightGrey: "#8E8Eff",
  green: "#28777A",
  black: "#131313",
  greyEBEBEB: "#EBEBEB",
  background: "#F8F8F8",
  yellow: "#FFBB00",
  gold: "#e6b02f",
  overlay: "#28367A96",
  med: "#17A589",
};
