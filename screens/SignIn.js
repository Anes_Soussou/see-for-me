import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Modal,
  ScrollView,
} from "react-native";
import Colors from "../constants/Colors";
import SuiviniButton from "../components/ui/SuiviniButton";
import SuiviniInput from "../components/ui/SuiviniInput";
import SuiviniLink from "../components/ui/SuiviniLink";
import SuiviniLogo from "../components/SuiviniLogo";
import SuiviniModal from "../components/SuiviniModal";
import { SafeAreaView } from "react-native-safe-area-context";
import KeyboardAvoidingWrapper from "../components/KeyboardAvoidingWrapper";
import axios from "axios";

const SignIn = function (props) {
  const initErrors = {
    emailError: "",
    passwordError: "",
  };
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errors, setErrors] = useState(initErrors);
  const [errorModalVisible, setErrorModalVisible] = useState(false);
  const [errorModalMessage, setErrorModalMessage] = useState("");

  const handleEmailChange = (value) => {
    setEmail(value);
    setErrors((errors) => ({ ...errors, emailError: null }));
  };

  const handlePasswordChange = (value) => {
    setPassword(value);
    setErrors((errors) => ({ ...errors, passwordError: null }));
  };

  const handleSignIn = async () => {
    let valid = true;
    if (!email) {
      valid = false;
      setErrors((errors) => ({
        ...errors,
        emailError: "Please enter your email",
      }));
    }
    if (!password) {
      valid = false;
      setErrors((errors) => ({
        ...errors,
        passwordError: "Please enter your password",
      }));
    }

    if (valid) {
      try {
        const response = await axios.post(
          "http://192.168.0.32:8000/api/login",
          {
            email: email,
            password: password,
          },
          {
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
            },
          }
        );

        if (response.status === 200) {
          console.log("User logged in successfully:", response.data);
          props.navigation.navigate("Home");
        } else {
          console.log("Error logging in:", response.data);
          setErrorModalVisible(true); // Show error modal
          setErrorModalMessage("Invalid email or password."); // Set error message
        }
      } catch (error) {
        console.error("An error occurred:", error);
        console.log("Error response:", error.response);
        setErrorModalVisible(true); // Show error modal
        setErrorModalMessage("Invalid email or password."); // Set error message
      }
    }
  };

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
  }, []);

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.azure }}>
      <KeyboardAvoidingWrapper style={{ backgroundColor: Colors.azure }}>
        <View
          style={{
            flex: 1,
            padding: 20,
            width: "100%",
            maxWidth: 400,
            alignSelf: "center",
          }}
        >
          <View
            style={{
              flex: 1.5,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <SuiviniLogo />
          </View>
          <View style={{ flex: 2 }}>
            <SuiviniInput
              placeholder={() => "Email"}
              onChangeText={handleEmailChange}
              keyboardType="email-address"
              autoCapitalize="none"
              errorMessage={errors.emailError}
              containerStyle={{ width: "100%", marginTop: 0 }}
              inputContainerStyle={{ width: "100%", marginTop: 0 }}
              value={email}
            />
            <SuiviniInput
              placeholder="Password"
              secureTextEntry={true}
              autoCapitalize="none"
              onChangeText={handlePasswordChange}
              containerStyle={{ width: "100%", marginTop: 10 }}
              inputContainerStyle={{ width: "100%", marginTop: 0 }}
              value={password}
              errorMessage={errors.passwordError}
            />
            {/* <View
                style={{
                  paddingHorizontal: errorText == '"Please Verify Email"' ? 10 : 70,
                  alignItems: 'center',
                  marginTop: 10,
                }}
              >
                {isRequesting ? <ActivityIndicator color={Colors.secondary} /> : <></>}
                {errorText ? (
                  errorText == '"Please Verify Email"' ? (
                    <View style={{ color: Colors.secondary }}>
                      <Text style={{ color: Colors.secondary }}>
                        Votre email n'est pas confirmé
                      </Text>
                      <Text style={{ color: Colors.secondary }}>Votre email est confirmé</Text>
                    </View>
                  ) : (
                    <Text style={{ color: Colors.secondary, textAlign: 'center' }}>
                      Mot de passe est invalid
                    </Text>
                  )
                ) : (
                  <></>
                )}
              </View> */}
            <SuiviniButton
              text="LOGIN TO ACCOUNT"
              onPress={handleSignIn}
              style={{ marginTop: 20, width: 200, alignSelf: "center" }}
              textStyle={{ color: "#fff", fontWeight: "bold", fontSize: 15 }}
            />
            <Text
              style={{
                color: "#000",
                //fontFamily: "Metropolis-Bold",
                marginTop: 5,
                textAlign: "center",
              }}
            >
              OR
            </Text>
            <SuiviniButton
              text="SIGN UP"
              onPress={() => {
                props.navigation.navigate("Register");
              }}
              style={{
                marginTop: 7,
                backgroundColor: Colors.primary,
                width: 200,
                alignSelf: "center",
              }}
              textStyle={{ color: "#fff", fontWeight: "bold", fontSize: 15 }}
            />
            <SuiviniLink
              text="Forgot password"
              onPress={() => {
                props.navigation.navigate("ForgotPassword");
              }}
              textStyle={{
                fontWeight: "bold",
                color: "#000",
              }}
            />
          </View>
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={errorModalVisible}
          onRequestClose={() => setErrorModalVisible(false)}
        >
          <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
              <View style={styles.modalHeader}>
                <Text style={styles.modalTitle}>Error</Text>
              </View>
              <View style={styles.modalBody}>
                <Text>{errorModalMessage}</Text>
              </View>
              <View style={styles.modalFooter}>
                <TouchableOpacity
                  onPress={() => setErrorModalVisible(false)}
                  style={[
                    styles.buttonTextStyle,
                    { backgroundColor: Colors.primary,borderRadius:5 },
                  ]}
                >
                  <Text
                    style={{
                      color: "#fff",
                      fontWeight: "bold",
                      fontSize: 15,
                      
                      justifyContent: "center",
                      alignItems: "center",
                      paddingLeft:20,
                      paddingRight:20
                    }}
                  >
                    Close
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </KeyboardAvoidingWrapper>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.primary,
  },
  form: {},
  modalContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#3e3e3ea1",
  },
  modalContent: {
    backgroundColor: "#fff",
    width: "95%",
    borderRadius: 5,
  },
  modalHeader: {
    height: 60,
    backgroundColor: Colors.secondary,
    alignItems: "center",
  },
  modalTitle: {
    color: "#fff",
    fontFamily: "Metropolis-Bold",
    fontSize: 17,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
  },
  modalBody: {
    paddingHorizontal: 30,
    paddingVertical: 20,
  },
  modalFooter: {
    paddingHorizontal: 30,
    paddingVertical: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  buttonTextStyle: {
    fontFamily: "Metropolis",
    backgroundColor: Colors.primary,
  },
});

export default SignIn;
