import React,{ useState } from "react";
import { Text, View, ScrollView, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../constants/Colors";
import { SafeAreaView } from "react-native-safe-area-context";

const Profile = ({ name, image, onEdit, onDelete }) => {
  return (
    <View style={styles.profileContainer}>
      <View style={styles.profileContent}>
        <Image source={image} style={styles.profileImage} />
        <Text style={styles.profileName}>{name}</Text>
        <View style={{marginRight:40,flexDirection:"row"}}>
        <TouchableOpacity onPress={onEdit}>
          <Ionicons name="pencil-outline" size={24} color={Colors.primary} />
        </TouchableOpacity>
        <TouchableOpacity onPress={onDelete}>
          <Ionicons name="trash-outline" size={24} color={Colors.secondary} />
        </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const Doctors = function (props) {
  const [doctors, setDoctors] = useState([
    { id: 1, name: "Dr. John Doe", image: require("../assets/imgs/doctor.png") },
    { id: 2, name: "Dr. Khalfallah MAYSSA", image: require("../assets/imgs/doctore.png") },
    { id: 3, name: "Dr. Helmi BEN SALEM", image: require("../assets/imgs/doctor.png") },
    { id: 4, name: "Dr. Ben rejeb OUSSAMA", image: require("../assets/imgs/doctor.png") },
    { id: 5, name: "Dr. Anissa GHARBI", image: require("../assets/imgs/doctore.png") },
  ]);

  const handleAddDoctor = () => {
    const newDoctor = { id: doctors.length + 1, name: "New Doctor", image: require("../assets/imgs/doctor.png") };
    setDoctors([...doctors, newDoctor]);
  };

  const handleEditDoctor = (id) => {
    // TODO: handle edit doctor logic
    console.log("Edit doctor with id:", id);
  };

  const handleDeleteDoctor = (id) => {
    const updatedDoctors = doctors.filter((doctor) => doctor.id !== id);
    setDoctors(updatedDoctors);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navbar}>
        <View style={styles.leftContainer}>
          <Ionicons name="menu" size={30} color="black" />
        </View>
        <View style={styles.centerContainer}>
          <Text style={styles.title}>Suivini</Text>
        </View>
        <View style={styles.rightContainer}>
          <Ionicons name="person-circle" size={30} color="black" />
        </View>
      </View>
      <View style={styles.contentContainer}>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.button} onPress={handleAddDoctor}>
            <Ionicons name="add-circle-outline" size={24} color="black" />
            <Text style={styles.buttonText}>Add new doctor</Text>
          </TouchableOpacity>
        </View>
        <ScrollView contentContainerStyle={styles.profileList}>
          {doctors.map((doctor) => (
            <Profile
              key={doctor.id}
              name={doctor.name}
              image={doctor.image}
              onEdit={() => handleEditDoctor(doctor.id)}
              onDelete={() => handleDeleteDoctor(doctor.id)}
              />
          ))}
               </ScrollView>
      </View>
    </SafeAreaView>


// const Doctors = function (props) {
//   return (
//     <SafeAreaView style={styles.container}>
//       <View style={styles.navbar}>
//         <View style={styles.leftContainer}>
//           <Ionicons name="menu" size={30} color="black" />
//         </View>
//         <View style={styles.centerContainer}>
//           <Text style={styles.title}>Suivini</Text>
//         </View>
//         <View style={styles.rightContainer}>
//           <Ionicons name="person-circle" size={30} color="black" />
//         </View>
//       </View>
//       <View style={styles.contentContainer}>
//         <ScrollView contentContainerStyle={styles.profileList}>
//           <Profile
//             name="Dr. John Doe"
//             image={require("../assets/imgs/doctor.png")}
//           />
//           <Profile
//             name="Dr. Khalfallah MAYSSA"
//             image={require("../assets/imgs/doctore.png")}
//           />
//           <Profile
//             name="Dr. Helmi BEN SALEM"
//             image={require("../assets/imgs/doctor.png")}
//           />

//           <Profile
//             name="Dr. Ben rejeb OUSSAMA"
//             image={require("../assets/imgs/doctor.png")}
//           />

//           <Profile
//             name="Dr. Anissa GHARBI"
//             image={require("../assets/imgs/doctore.png")}
//           />
//           {/* Add more profiles here */}
//         </ScrollView>
//       </View>
    // </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E7E7E7",
  },
  navbar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    height: 60,
    backgroundColor: "#E7E7E7",
    marginTop: 10,
  },
  contentContainer: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 16,
  },
  profileList: {
    paddingBottom: 10,
    marginTop: 25,
  },
  leftContainer: {
    flex: 1,
    alignItems: "flex-start",
  },
  centerContainer: {
    flex: 1,
    alignItems: "center",
  },
  rightContainer: {
    flex: 1,
    alignItems: "flex-end",
  },
  title: {
    fontSize: 23,
    fontWeight: "bold",
    color: Colors.med,
  },
  profileContainer: {
    marginBottom: 10,
  },
  profileContent: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: "#ffff",
    borderRadius: 10,
    borderColor: Colors.primary,
    borderWidth: 1,
  },
  profileImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10,
  },
  profileName: {
    fontSize: 16,
    fontWeight: "bold",
    color: Colors.primary,
  },
});

export default Doctors;
