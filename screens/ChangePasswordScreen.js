/**
 * Component for a forgotten password feature. It includes form inputs
 * to collect the user's email address, validation of the email input,
 * and a button to send a password recovery email to the user.
 */
import React, { useState } from 'react';
import { ScrollView, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import Colors from '../constants/Colors';
import { Modal } from 'react-native';
import SuiviniInput from '../components/ui/SuiviniInput';
import SuiviniButton from '../components/ui/SuiviniButton';

const ChangePasswordScreen = function (props) {
  // message modal
  const [modalVisible, setModalVisible] = useState(false);
  const [message, setMessage] = useState({
    text: '',
    type: '',
  });
  const closeModal = () => {
    setModalVisible(false);
  };
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.form}>
          <Text style={styles.modalTitle}>Change password</Text>
          <SuiviniInput
            placeholder={'New Password'}
            value={password}
            onChangeText={setPassword}
            autoCapitalize="none"
            containerStyle={{ width: '100%' }}
            inputContainerStyle={{ width: '100%' }}
          />
          <SuiviniInput
            placeholder={'Confirm new Password'}
            value={passwordConfirmation}
            onChangeText={setPasswordConfirmation}
            autoCapitalize="none"
            containerStyle={{ width: '100%' }}
            inputContainerStyle={{ width: '100%' }}
          />
        </View>
        <SuiviniButton
          style={styles.button}
          text='VALIDATE'
          onPress={() => props.navigation.navigate("Profile")}
          textStyle={styles.buttonText}
        />
      </ScrollView>

      {/* Message Modal  */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={closeModal}
        style={{ justifyContent: 'center' }}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <View style={styles.modalBody}>
              <Text style={styles.modalTitle}>{message.text}</Text>
            </View>
            <View style={styles.modalFooter}>
              <SuiviniButton
                text='CLOSE'
                textStyle={{ textTransform: 'none' }}
                style={{
                  backgroundColor: Colors.primary,
                  borderColor: Colors.primary,
                  paddingHorizontal: 20,
                  paddingVertical: 0,
                  height: 40,
                }}
                onPress={closeModal}
              />
              {message.type != 'info' && (
                <SuiviniButton
                  text='VALIDATE'
                  textStyle={{ textTransform: 'none' }}
                  style={{
                    backgroundColor: Colors.secondary,
                    borderColor: Colors.secondary,
                    paddingHorizontal: 20,
                    paddingVertical: 0,
                    height: 40,
                  }}
                  onPress={() => props.navigation.navigate("Profile")}
                />
              )}
            </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },
  container: {
    flexGrow: 1,
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 40,
    paddingVertical: 40,
    backgroundColor: "#fff",
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "flex-start",
  },
  headerTag: {
    backgroundColor: Colors.secondary,
    borderRadius: 14,
    width: 28,
    height: 28,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  headerTagNumber: { color: "#fff", fontSize: 17, fontWeight: "bold" },
  headerTitle: {
    fontSize: 17,
    fontWeight: "bold",
    color: Colors.primary,
  },
  form: { flexGrow: 1, justifyContent: "center" },
  button: {
    backgroundColor: Colors.primary,
    height: 48,
    width: 295,
    paddingLeft: 20,
    paddingRight: 10,
    alignSelf: "center",
    marginTop: 30,
    justifyContent: "center",
    flexDirection: "row",
  },
  buttonText: {
    textTransform: "none",
    flex: 1,
    textAlign: "center",
    fontWeight: "bold",
    letterSpacing: 2,
  },
  modalContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#3e3e3ea1",
  },
  modalContent: {
    backgroundColor: "#fff",
    width: "95%",
    borderRadius: 5,
  },
  modalHeader: { justifyContent: "center", alignItems: "center" },
  modalTitle: {
    fontSize: 16,
    fontWeight: "bold",
    color: Colors.primary,
  },
  modalBody: {
    paddingHorizontal: 30,
    paddingVertical: 20,
  },
  modalFooter: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
});

export default ChangePasswordScreen;
