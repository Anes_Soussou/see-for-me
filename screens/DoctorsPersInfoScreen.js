
import React from "react";
import { TouchableOpacity } from "react-native";
import { StyleSheet, Text, View, Image } from "react-native";
import Colors from "../constants/Colors";
import AddressIcon from "../components/svg/AddressIcon";
import EmailIcon from "../components/svg/EmailIcon";
import PhoneIcon from "../components/svg/PhoneIcon";
import SuiviniButton from "../components/ui/SuiviniButton";
import { Ionicons } from "@expo/vector-icons";



const DoctorsPersInfoScreen = function (props) {

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#fff",
        paddingHorizontal: 5,
        paddingVertical: 5,
      }}
    >
      <View style={{ flex: 1 }}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginVertical: 5,
            paddingBottom: 10,
            borderBottomWidth: 1,
            borderColor: Colors.grey,
          }}
        >
          <View
            style={{
              width: 55,
              height: 55,
              backgroundColor: Colors.green,
              borderRadius: 40,
              alignItems: "center",
              justifyContent: "center",
              borderColor: "#fff",
              borderWidth: 2,
              marginTop: 15,
            }}
          >
            <Text
              style={{
                fontSize: 20,
                color: "#fff",

                textTransform: "uppercase",
              }}
            >
              DJ
            </Text>
          </View>

          <View style={{ flexDirection: "column", flex: 2 }}>
            <Text
              style={{
                color: Colors.black,
                marginLeft: 5,
                fontSize: 20,
                marginTop: 15,
              }}
              numberOfLines={3}
              ellipsizeMode="tail"
            >
              Dr. John Doe
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 15,
              marginRight: 10,
            }}
          >
            <Ionicons
              name="md-person-add"
              size={23}
              color={Colors.grey}
              style={{ marginRight: 25 }}
            />
            <Ionicons name="md-chatbubbles" size={23} color={Colors.grey} />
          </View>
        </View>

        <View style={styles.menuWrapper}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <AddressIcon
              width="35"
              height="21"
              style={{ paddingHorizontal: 8 }}
            />
            <TouchableOpacity style={[styles.valueAdress]}>
              <Text style={styles.valueText} numberOfLines={2}>
                Av 14 janvier-Sousse{" "}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <EmailIcon width="21" height="14" style={{ marginHorizontal: 8 }} />
            <TouchableOpacity style={[styles.valueAdress]}>
              <Text style={[styles.valueText]}>john.Doe@gmail.com</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <PhoneIcon width="21" height="21" style={{ marginHorizontal: 8 }} />
            <TouchableOpacity style={[styles.valueAdress]}>
              <Text style={[styles.valueText]}>+33235123632</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={styles.labelText}>Degrees: </Text>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={styles.valueTextDegree}>
              Diploma of doctor of medicine of the faculty of medicine of
              Sousse.{"\n"}
              Capacity in Gerontology and Geriatrics of the faculty of medicine
              Diderot Paris VII.{"\n"}
              <Text style={{ fontWeight: "bold" }}>Languages: </Text>English and
              French
            </Text>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={styles.labelText}>Medical insurance: </Text>
          </View>
          <View style={{ marginTop: 10 }}>
            <Image
              source={require("../assets/imgs/cnam.jpg")}
              style={{ width: 150, height: 80 }}
              resizeMode="contain"
            />
          </View>
          <View style={styles.buttonContainer}>
            <SuiviniButton
              text="Make an appointment now"
              textStyle={styles.buttonTextStyle}
              style={{
                ...styles.button,
                backgroundColor: Colors.med,
              }}
              onPress={()=> props.navigation.navigate('CalendarScreen')}
            />
            <PhoneIcon width="21" height="21" style={{ marginHorizontal: 8 }} />
            <TouchableOpacity
              style={[styles.valueAdress]}
            >
              <Text style={[styles.valueText]}>
         Calendar
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  // Modal
  modal: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.overlay,
  },

  buttonText: { textTransform: "none", flex: 1, textAlign: "center" },
  // menu
  menuWrapper: {
    marginHorizontal: 5,
    flex: 1,
  },
  container: {
    flex: 1,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    // paddingTop: 10,
    //  marginTop: 10,
  },
  value: {
    height: 40,
    justifyContent: "center",
    backgroundColor: Colors.background,
    paddingHorizontal: 5,
  },
  valueAdress: {
    height: 35,
    width: 305,
    borderRadius: 10,
    justifyContent: "center",
    backgroundColor: Colors.background,
    flex: 1,
    marginVertical: 5,
  },
  valueText: {
    fontSize: 14,
    color: "#515151",

    marginLeft: 20,
  },
  labelText: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 15,
    marginLeft: 5,
    color: Colors.green,
  },
  valueTextDegree: {
    fontSize: 16,
    marginTop: 10,
    marginLeft: 13,
    lineHeight: 20,
  },
  button: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 55,
    width: 290,
    marginTop: 40,
    alignSelf: "center",
  },
  buttonTextStyle: {
    fontSize: 17,
    textTransform: "none",
    fontWeight: "bold",
  },
});

export default DoctorsPersInfoScreen;
