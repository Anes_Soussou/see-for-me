import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../constants/Colors";

const NotificationsA = function (props) {
  const [notifications, setNotifications] = useState([
    {
      message:
        "Guirat Chayma asked for an appointment !",
      iconName: "md-calendar",
      iconColor: "#FF6347",
      pressed: false,
    },
    {
      message:
        "Mansour Meriem asked for an appointment !",
      iconName: "md-calendar",
      iconColor: "#FF6347",
      pressed: false,
    },
    {
      message:
        "Mohamed Rym asked for an appointment !",
      iconName: "md-calendar",
      iconColor: "#FF6347",
      pressed: false,
    },
  ]);

  const handleNotificationPress = (index) => {
    setNotifications(
      (prevNotifications) => {
        const updatedNotifications = [...prevNotifications];
        updatedNotifications[index] = {
          ...updatedNotifications[index],
          pressed: !updatedNotifications[index].pressed,
        };
        return updatedNotifications;
      },
   

    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navbar}>
        <Ionicons name="md-notifications" size={30} style={styles.icon} />
        <Text style={styles.title}>My notifications</Text>
      </View>
      <ScrollView>
        {notifications.map((notification, index) => (
          <TouchableOpacity
            style={[
              styles.notificationButton,
              notification.pressed && styles.notificationButtonPressed,
            ]}
            onPress={()=> props.navigation.navigate('Decision')}
            activeOpacity={0.8}
            key={index}
          >
            <Ionicons
              name={notification.iconName}
              size={24}
              color={notification.iconColor}
              style={styles.notificationIcon}
            />
            <Text style={styles.notificationText}>{notification.message}</Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E7E7E7",
  },
  navbar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingHorizontal: 16,
    height: 80,
    backgroundColor: "#fff",
  },
  icon: {
    marginRight: 10,
    marginLeft: 10,
    color: "#FFD700",
    marginTop: 15,
  },
  title: {
    fontSize: 22,
    fontWeight: "bold",
    marginTop: 15,
  },
  notificationButton: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 15,
    borderBottomColor: "#E7E7E7",
    paddingVertical: 25,
    backgroundColor: "white",
    marginTop : 1 ,
  },
  notificationButtonPressed: {
    backgroundColor: "#fff",
  },
  notificationIcon: {
    marginRight: 10,
    marginLeft: 5,
  },
  notificationText: {
    fontSize: 16,
    flex: 1,
    marginLeft: 5,
  },
});

export default NotificationsA;
