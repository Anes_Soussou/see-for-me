import React, { useEffect, useState } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import SuiviniButton from "../components/ui/SuiviniButton";
import Colors from "../constants/Colors";
import { Ionicons } from "@expo/vector-icons";
import PhoneIcon from "../components/svg/PhoneIcon";

const Home = (props) => {
  useEffect(() => {
    // Request Barcode Scanner permissions here
  }, []);

  const { userType } = props.route.params || { userType: "" };
  console.log('user',userType)

  const [showSubMenu, setShowSubMenu] = useState(false);

  const toggleSubMenu = () => {
    setShowSubMenu(!showSubMenu);
  };

  return (
    <View style={styles.container}>
      <View style={styles.navbar}>
        <TouchableOpacity
          style={styles.leftContainer}
          onPress={() => {
            toggleSubMenu(); // Call the toggleSubMenu function
          }}
        >
          <Ionicons name="menu" size={30} color="black" />
        </TouchableOpacity>

        <View style={styles.centerContainer}>
          <Text style={styles.title}>SEE FOR ME</Text>
        </View>
        <TouchableOpacity
          style={styles.rightContainer}
          onPress={() =>
            props.navigation.navigate("Profile", { screen: "Profile" })
          }
        >
          <Ionicons name="person-circle" size={30} color="black" />
        </TouchableOpacity>
      </View>
      {userType === "bénévole" ? (
        <View>
          <View style={styles.contentContainerAssitant}>
            <Text style={styles.titleQR}>
              Making a difference by providing assistance to blind individuals
              is a rewarding and impactful way to contribute to the inclusivity
              and empowerment of those with visual impairments
            </Text>
          </View>
          {showSubMenu && (
            <View style={styles.subMenuContainer}>
              <TouchableOpacity
                style={styles.subMenuItem}
                onPress={() => {
                  // Handle the "Adding to family list" action here
                  toggleSubMenu(); // Close the submenu
                }}
              >
                <Text style={styles.subMenuItemText}>
                  Adding to family list
                </Text>
              </TouchableOpacity>
              {/* Add more submenu items here if needed */}
            </View>
          )}
        </View>
      ) : (
        <View style={styles.contentContainer}>
          <Text style={styles.titleQR}>Start a video call</Text>
          <SuiviniButton
            text="Start Video Call"
            textStyle={styles.buttonTextStyle}
            style={styles.button}
            iconLeft={() => (
              <PhoneIcon
                style={{ marginRight: 20 }}
                height="29"
                width="35"
                stroke="#FFF"
              />
            )}
            onPress={() =>
              props.navigation.navigate("Patients", { screen: "Patients" })
            }
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E7E7E7",
  },
  navbar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    height: 100,
    backgroundColor: "#E7E7E7",
  },
  leftContainer: {},
  centerContainer: {
    flex: 1,
    alignItems: "center",
  },
  title: {
    fontSize: 23,
    fontWeight: "bold",
    color: Colors.med,
    marginTop: 10,
  },
  rightContainer: {},
  contentContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  contentContainerAssitant: {
    marginVertical: 150,
    alignItems: "center",
    justifyContent: "center",
  },
  titleQR: {
    fontSize: 23,
    fontWeight: "bold",
    alignItems: "center",
    justifyContent: "center",
    maxWidth: "90%",
    textAlign: "center",
    marginTop: 25,
  },
  button: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: 250,
    height: 250,
    borderRadius: 125,
    marginTop: 40,
    margin: 50,
  },
  buttonTextStyle: {
    fontSize: 20,
    textTransform: "none",
    color: "#FFF",
    fontWeight: "bold",
  },
  subMenuContainer: {
    position: "absolute",
    right: 180,
    backgroundColor: Colors.med,
    borderRadius: 5,
    shadowColor: "white",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 3,
    zIndex: 1,
  },
  subMenuItem: {
    padding: 10,
  },
  subMenuItemText: {
    fontSize: 16,
    color: "white",
  },
});

export default Home;
