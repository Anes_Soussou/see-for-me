import {React, useEffect} from "react";
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../constants/Colors";
import { SafeAreaView } from "react-native-safe-area-context";
// import Daily from '@daily-co/react-native-daily-js';

// const Profile = ({ name, image }) => {


//   return (
//     <View style={styles.profileContainer}>
//       <View style={styles.profileContent}>
//         <Image source={image} style={styles.profileImage} />
//         <Text style={styles.profileName}>{name}</Text>
//       </View>
//     </View>
//   );
// };

const Patients = function (props) {
  //   useEffect(() => {
  //   const call = Daily.createCallObject();
  // call.join({ url: 'https://your-team.daily.co/allhands' });
  // }, []);
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navbar}>
        <View style={styles.leftContainer}>
          <Ionicons name="menu" size={30} color="black" />
        </View>
        <View style={styles.centerContainer}>
          <Text style={styles.title}>SEE FOR ME</Text>
        </View>
        <View style={styles.rightContainer}>
          <Ionicons name="person-circle" size={30} color="black" />
        </View>
      </View>
      <View style={styles.contentContainer}>

      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E7E7E7",
  },
  navbar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    height: 60,
    backgroundColor: "#E7E7E7",
    marginTop: 10,
  },
  contentContainer: {
    flex: 1,
    padding: 16,
    marginTop: 25,
  },
  profileList: {
    paddingBottom: 10,
    marginTop: 10,
  },
  leftContainer: {
    flex: 1,
    alignItems: "flex-start",
  },
  centerContainer: {
    flex: 1,
    alignItems: "center",
  },
  rightContainer: {
    flex: 1,
    alignItems: "flex-end",
  },
  title: {
    fontSize: 23,
    fontWeight: "bold",
    color: Colors.med,
  },
  profileContainer: {
    marginBottom: 10,
  },
  profileContent: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: "#fff",
    borderRadius: 10,
    borderColor: Colors.primary,
    borderWidth: 1,
  },
  profileImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10,
  },
  profileName: {
    fontSize: 16,
    fontWeight: "bold",
    marginLeft: 5,
    color: Colors.primary,
  },
  doctorText: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
    marginLeft: 7,
  },
});

export default Patients;
