import React, { useState } from "react";
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  Platform,
} from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import SuiviniButton from "../components/ui/SuiviniButton";
import { ScrollView } from "react-native";

const CalendarScreen = () => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [showStartDatePicker, setShowStartDatePicker] = useState(false);
  const [showEndDatePicker, setShowEndDatePicker] = useState(false);
  const [appointments, setAppointments] = useState([]);

  const onStartDateChange = (event, selectedDate) => {
    const currentDate = selectedDate || startDate;
    setShowStartDatePicker(Platform.OS === "ios");
    setStartDate(currentDate);
  };

  const onEndDateChange = (event, selectedDate) => {
    const currentDate = selectedDate || endDate;
    setShowEndDatePicker(Platform.OS === "ios");
    setEndDate(currentDate);
  };

  const showStartDatepicker = () => {
    setShowStartDatePicker(true);
  };

  const showEndDatepicker = () => {
    setShowEndDatePicker(true);
  };

  const saveAppointment = () => {
    console.log(`Title: ${title}`);
    console.log(`Description: ${description}`);
    console.log(`Start Date: ${startDate}`);
    console.log(`End Date: ${endDate}`);
    const newAppointment = {
      title: title,
      description: description,
      startDate: startDate,
      endDate: endDate,
    };
    setAppointments([...appointments, newAppointment]);
    setTitle("");
    setDescription("");
    setStartDate(new Date());
    setEndDate(new Date());
  };

  return (
    <ScrollView>
    <View style={styles.container}>
      <Text style={styles.label}>Title:</Text>
      <TextInput
        style={styles.input}
        value={title}
        onChangeText={(text) => setTitle(text)}
      />
      <Text style={styles.label}>Description:</Text>
      <TextInput
        style={styles.input}
        value={description}
        onChangeText={(text) => setDescription(text)}
      />
      <View style={styles.datePickerContainer}>
        <Text style={styles.label}>Start Date:</Text>
        <SuiviniButton
          textStyle={styles.buttonTextStyle}
          style={{
            ...styles.button,
            backgroundColor: Colors.med,
          }}
          onPress={showStartDatepicker}
          text={startDate.toDateString()}
        />
        {showStartDatePicker && (
          <DateTimePicker
            value={startDate}
            mode="date"
            display="spinner"
            onChange={onStartDateChange}
          />
        )}
      </View>
      <View style={styles.datePickerContainer}>
        <Text style={styles.label}>End Date:</Text>
        <SuiviniButton
          textStyle={styles.buttonTextStyle}
          style={{
            ...styles.button,
            backgroundColor: Colors.med,
          }}
          onPress={showEndDatepicker}
          text={endDate.toDateString()}
        />
        {showEndDatePicker && (
          <DateTimePicker
            value={endDate}
            mode="date"
            display="spinner"
            onChange={onEndDateChange}
          />
        )}
      </View>
      <SuiviniButton
        textStyle={styles.buttonTextStyle}
        style={{
          ...styles.saveButton,
          backgroundColor: Colors.secondary,
        }}
        onPress={saveAppointment}
        text="SAVE"
      />
      <View style={styles.appointmentContainer}>
        {appointments.map((appointment, index) => (
          <View style={styles.appointment} key={index}>
            <Text style={styles.appointmentTitle}>{appointment.title}</Text>
            <Text style={styles.appointmentDescription}>
              {appointment.description}
            </Text>
            <Text style={styles.appointmentDate}>
              {appointment.startDate.toDateString()} - {appointment.endDate.toDateString()}
            </Text>
          </View>
        ))}
      </View>
    </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  label: {
    fontWeight: "bold",
    marginBottom: 8,
  },
  input: {
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 4,
    padding: 8,
    marginBottom: 16,
  },
  datePickerContainer: {
    marginBottom: 16,
  },
  button: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 55,
    width: 290,
    marginTop: 10,
    alignSelf: "center",
  },
  buttonTextStyle: {
    fontSize: 17,
    textTransform: "none",
    fontWeight: "bold",
  },
  saveButton: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 55,
    width: 290,
    marginTop: 100,
    alignSelf: "center",
  },
  appointmentContainer: {
    padding: 16,
    marginBottom: 16,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 4,
  },
  appointment: {
    marginBottom: 8,
  },
  appointmentTitle: {
    fontWeight: 'bold',
  },
  appointmentDescription: {
    color: '#555',
  },
  appointmentDate: {
    color: '#888',
  },
});

export default CalendarScreen;
