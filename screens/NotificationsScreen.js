import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../constants/Colors";

const Notifications = function (props) {
  const [notifications, setNotifications] = useState([
    {
      message: "It's time to take your medication",
      iconName: "md-medkit",
      iconColor: Colors.primary,
      pressed: false,
    },
    {
      message: "Your appointment with Dr. John Doe has been validated.",
      iconName: "md-calendar",
      iconColor: "#FF6347",
      pressed: false,
    },
    {
      message: "Don't forget to practice good hygiene",
      iconName: "md-body",
      iconColor: Colors.med,
      pressed: false,
    },
    {
      message: "Your appointment with Dr. John Doe has been rejected.",
      iconName: "md-calendar",
      iconColor: "#FF6347",
      pressed: false,
    },
    {
      message: "Don't forget to stay hydrated",
      iconName: "md-body",
      iconColor: Colors.med,
      pressed: false,
    },
    {
      message: "It's time to take your medication",
      iconName: "md-medkit",
      iconColor: Colors.primary,
      pressed: false,
    },
    {
      message: "It's time to take your medication",
      iconName: "md-medkit",
      iconColor: Colors.primary,
      pressed: false,
    },
    {
      message:
        "Your appointment with Dr. Khalfallah Mayssa has been validated.",
      iconName: "md-calendar",
      iconColor: "#FF6347",
      pressed: false,
    },
    {
      message: "Don't forget to sleep early",
      iconName: "md-body",
      iconColor: Colors.med,
      pressed: false,
    },
  ]);

  const handleNotificationPress = (index) => {
    setNotifications(
      (prevNotifications) => {
        const updatedNotifications = [...prevNotifications];
        updatedNotifications[index] = {
          ...updatedNotifications[index],
          pressed: !updatedNotifications[index].pressed,
        };
        return updatedNotifications;
      },
    
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navbar}>
        <Ionicons name="md-notifications" size={30} style={styles.icon} />
        <Text style={styles.title}>My notifications</Text>
      </View>
      <ScrollView>
        {notifications.map((notification, index) => (
          <TouchableOpacity
            style={[
              styles.notificationButton,
              notification.pressed && styles.notificationButtonPressed,
            ]}
            onPress={() => handleNotificationPress(index)}
            activeOpacity={0.8}
            key={index}
          >
            <Ionicons
              name={notification.iconName}
              size={24}
              color={notification.iconColor}
              style={styles.notificationIcon}
            />
            <Text style={styles.notificationText}>{notification.message}</Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E7E7E7",
  },
  navbar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingHorizontal: 16,
    height: 80,
    backgroundColor: "#fff",
  },
  icon: {
    marginRight: 10,
    marginLeft: 10,
    color: "#FFD700",
    marginTop: 15,
  },
  title: {
    fontSize: 22,
    fontWeight: "bold",
    marginTop: 15,
  },
  notificationButton: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: "#fff",
    marginTop: 1,
    height: 90,
  },
  notificationButtonPressed: {
    backgroundColor: "#E7E7E7",
  },
  notificationIcon: {
    marginRight: 10,
    marginLeft: 5,
  },
  notificationText: {
    fontSize: 16,
    flex: 1,
    marginLeft: 5,
  },
});

export default Notifications;
