/**
 * The Tab navigation allows the user to navigate between
 * Home, Appointments, Medical Reords, Doctors Screens
 */
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeNavigation from './HomeNavigation';
import TabBarContent from './TabBarContent';

const Tabs = createBottomTabNavigator();

const TabNavigation = (props) => (
  <Tabs.Navigator tabBar={(props) => <TabBarContent {...props} />}>
    <Tabs.Screen name="Home" component={HomeNavigation} options={{ tabBarLabel:'Home'}} />
      <Tabs.Screen
        name="Appointments"
        // component={AppointmentsNavigation}
        options={{ tabBarLabel: 'Appointments', unmountOnBlur: true }}
      />
      <Tabs.Screen
        name="Doctors"
        // component={DoctorsNavigation}
        options={{ tabBarLabel: 'Doctors', unmountOnBlur: true }}
      />

    <Tabs.Screen
      name="MedicalRecord"
      // component={MedicalRecordsNavigation}
      options={{ tabBarLabel: 'MedicalRecord', unmountOnBlur: true }}
    />
      <Tabs.Screen
        name="Medicines"
        // component={MedicinesNavigation}
        options={{ tabBarLabel:'Medicines', unmountOnBlur: true }}
      />

    <Tabs.Screen
      name="Account"
      // component={AccountNavigation}
      options={{ tabBarLabel:'ACCOUNT', unmountOnBlur: true }}
    />
  </Tabs.Navigator>
);

export default TabNavigation;
