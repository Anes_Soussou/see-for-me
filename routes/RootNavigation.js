/**
 * This component serves as the entry point for our navigation structure.
 */
import React, { useEffect, useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import DrawerNavigation from './DrawerNavigation';
import Register from "../screens/Register";



const Stack = createStackNavigator();
const RootNavigation = (props) => {
  const [loading, setLoading] = useState(true);


  const getComponent = () => {
    return <DrawerNavigation />;
  };

  return (
    <Stack.Navigator headerMode="none">
          <Stack.Screen
            name={'App'}
            component={getComponent}
            options={{
              animationEnabled: false,
            }}
          />
        <Stack.Screen
          name="Account"
          component={Register}
          options={{
            animationEnabled: false,
          }}
        />
    </Stack.Navigator>
  );
};

export default RootNavigation;

