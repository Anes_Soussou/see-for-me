/* defines a loading screen component */
import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import Colors from '../constants/Colors';
import { ActivityIndicator } from 'react-native';

const Loading = () => (
  <View
    style={{
      flex: 1,
      backgroundColor: Colors.primary,
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    <ActivityIndicator color={Colors.secondary} size="large" />
  </View>
);
const Stack = createStackNavigator();
const LoadingScreen = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Loading"
      component={Loading}
      options={{ headerMode: 'none', headerShown: false }}
    />
  </Stack.Navigator>
);

export default LoadingScreen;
