/**
 * The component displays a HomeScreen and also provides navigation options
 * such as a logo, menu icon and an account icon.
 */
import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Icon } from "@rneui/themed";
import { Keyboard, Platform, View } from "react-native";

import Home from "../screens/Home";

const Stack = createStackNavigator();
const HomeNavigation = (props) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Homes"
        component={Home}
        options={{
          title: null,
          headerTitleContainerStyle: {
            height: 56,
            justifyContent: "center",
            alignItems: "center",
          },
          headerTitle: () => <Text>Suivini</Text>,
          headerLeft: () => (
            <Icon
              containerStyle={{
                width: 60,
                paddingVertical: 5,
              }}
              type="ionicon"
              name={Platform.OS === "ios" ? "ios-menu" : "md-menu"}
              color="#28367A"
              onPress={() => {
                Keyboard.dismiss();
                props.navigation.openDrawer();
              }}
              size={35}
            />
          ),
          headerRight: () => (
            <View style={{ flexDirection: "row", paddingRight: 20 }}>
              <Icon
                containerStyle={{
                  width: 40,
                  paddingVertical: 5,
                }}
                type="ionicon"
                name={"person-circle-sharp"}
                color="#28367A"
                onPress={() => {
                  Keyboard.dismiss();
                  props.navigation.navigate("Account");
                }}
                size={35}
              />
            </View>
          ),
        }}
      />
    </Stack.Navigator>
  );
};

export default HomeNavigation;
