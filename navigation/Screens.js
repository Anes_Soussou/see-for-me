import { Animated, Dimensions, Easing } from "react-native";
// header for screens
import { Header, Icon } from "../components";
import { argonTheme, tabs } from "../constants";

import Articles from "../screens/Articles";
import { Block } from "galio-framework";
// drawer
import CustomDrawerContent from "./Menu";
import Elements from "../screens/Elements";
// screens
import Home from "../screens/Home";
import TabBarContent from "../routes/TabBarContent";
import Onboarding from "../screens/Onboarding";
import Pro from "../screens/Pro";
import Profile from "../screens/Profile";
import ProfileAssistant from "../screens/ProfileAssistant";
import React from "react";
import Register from "../screens/Register";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createStackNavigator } from "@react-navigation/stack";
import SignIn from "../screens/SignIn";
import ForgotPassword from "../screens/ForgotPassword";
import DrawerNavigation from "../routes/DrawerNavigation";
import Appointments from "../screens/Appointments";
import Doctors from "../screens/Doctors";
import MyAppointments from "../screens/MyAppointments";
import MedicalRecords from "../screens/MedicalRecords";
import Medicines from "../screens/Medicines";
import DoctorsInfoScreen from "../screens/DoctorsInfoScreen";
import DoctorsPersInfoScreen from "../screens/DoctorsPersInfoScreen";
import ChangePassword from "../screens/ChangePasswordScreen";
import Messages from "../screens/MessagesScreen";
import Notifications from "../screens/NotificationsScreen";
import NotificationsA from "../screens/NotificationsA";
import Decision from "../screens/Decision";
import Patients from "../screens/Patients";
import Ordonnance from "../screens/Ordonnance";

//import CalendarScreen from "../screens/CalendarScreen";
import Conversation from "../screens/Conversation";
import CalendarScreen from "../screens/CalendarScreen";


const { width } = Dimensions.get("screen");

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const getComponent = () => {
  return <DrawerNavigation />;
};
const Tab = createBottomTabNavigator();

function HomeStack(props) {
  return (
    <Stack.Navigator
      screenOptions={{
        mode: "card",
        headerShown: "screen",
      }}
    >
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          header: ({ navigation, scene }) => (
            <Header
              title="Home"
              search
              options
              navigation={navigation}
              scene={scene}
            />
          ),
          cardStyle: { backgroundColor: "#F8F9FE" },
        }}
      />
      {/* <Stack.Screen
        name={"DrawerContent"}
        component={getComponent}
        options={{
          animationEnabled: false,
        }}
      />
      <Stack.Screen
        name="TabBarContent"
        component={TabBarContent}
        options={{
          header: ({ navigation, scene }) => (
            <Header
              title="TabBarContent"
              search
              options
              navigation={navigation}
              scene={scene}
            />
          ),
          cardStyle: { backgroundColor: "#F8F9FE" },
        }}
      /> */}
    </Stack.Navigator>
  );
}

export default function OnboardingStack(props) {
  return (
    <Stack.Navigator
      screenOptions={{
        mode: "card",
        headerShown: false,
      }}
    >
      <Stack.Screen
        name="Onboarding"
        component={Onboarding}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="SignIn"
        component={SignIn}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="Appointments"
        component={Appointments}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="MyAppointments"
        component={MyAppointments}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="MedicalRecords"
        component={MedicalRecords}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="Doctors"
        component={Doctors}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="DoctorsInfoScreen"
        component={DoctorsInfoScreen}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="DoctorsPersInfoScreen"
        component={DoctorsPersInfoScreen}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="Medicines"
        component={Medicines}
        option={{
          headerTransparent: true,
        }}
      />
      {/* <Stack.Screen
        name={"DrawerContent"}
        component={getComponent}
        options={{
          animationEnabled: false,
        }}
      />
      <Stack.Screen
        name="TabBarContent"
        component={TabBarContent}
        option={{
          headerTransparent: true,
        }}
      /> */}
      <Stack.Screen
        name="Profile"
        component={Profile}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="ProfileAssistant"
        component={ProfileAssistant}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="ChangePassword"
        component={ChangePassword}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="Messages"
        component={Messages}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="Notifications"
        component={Notifications}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="NotificationsA"
        component={NotificationsA}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="Conversation"
        component={Conversation}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="Decision"
        component={Decision}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="Patients"
        component={Patients}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="Ordonnance"
        component={Ordonnance}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen
        name="CalendarScreen"
        component={CalendarScreen}
        option={{
          headerTransparent: true,
        }}
      />
      <Stack.Screen name="App" component={AppStack} />
    </Stack.Navigator>
  );
}

function AppStack(props) {
  return (
    <Drawer.Navigator
      style={{ flex: 1 }}
      drawerContent={(props) => <CustomDrawerContent {...props} />}
      drawerStyle={{
        backgroundColor: "white",
        width: width * 0.8,
      }}
      drawerContentOptions={{
        activeTintcolor: "white",
        inactiveTintColor: "#000",
        activeBackgroundColor: "transparent",
        itemStyle: {
          width: width * 0.75,
          backgroundColor: "transparent",
          paddingVertical: 16,
          paddingHorizonal: 12,
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center",
          overflow: "hidden",
        },
        labelStyle: {
          fontSize: 18,
          marginLeft: 12,
          fontWeight: "normal",
        },
      }}
      initialRouteName="Home"
    >
      <Drawer.Screen
        name="Home"
        component={HomeStack}
        options={{
          headerShown: false,
        }}
      />
      <Drawer.Screen
        name="Account"
        component={Register}
        options={{
          headerShown: false,
        }}
      />
    </Drawer.Navigator>
  );
}
